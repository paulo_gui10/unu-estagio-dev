Paulo Guilherme Silva Albuquerque
pgsa@ecomp.poli.br

Front-end:
	Construção das páginas HTML de login e cadastro, utilizando bootstrap por questões estéticas e organizacionais e angularJS
para funcionalidades e validações do preenchimento dos formulários, assim como envio de informações para o arquivo Javascript,
armazenando-os em um objeto.
 	Dentre as ferramentas do AngularJS, destaco o "ng-required", que, combinado com "ng-disabled", garante que o botão de confirmação
só fique habilitado, quando determinados campos forem preenchidos. Com "ng-minlength" garanto um número mínimo de caracteres para a senha
e com "ng-pattern" manipulo especificamente a quantidade, ou o caractere que deve ter naquele campo.

Back-end(quase):
	Adquirir o Slim framework, através do composer, rodei, com uso do Apache, uma aplicação teste, busquei alguns conceitos sobre essa
framework, porém não obtive êxito na elaboração da resolução do problema. 